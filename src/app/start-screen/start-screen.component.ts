import { Component, OnInit } from '@angular/core';
import { GameService } from '../services';

@Component({
  selector: 'tsb-start-screen',
  templateUrl: './start-screen.component.html',
  styleUrls: ['./start-screen.component.css']
})
export class StartScreenComponent implements OnInit {

  constructor(private gameService: GameService) { }

  ngOnInit() {
  }

  startGame() {
    this.gameService.start();
  }

}
