import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Component, Input } from '@angular/core';
import { AppComponent } from './app.component';
import { GameService, ScoresService } from './services';


@Component({
  selector: 'tsb-game-area',
  template: ''
})
class MockGameAreaComponent { }

@Component({
  selector: 'tsb-start-screen',
  template: ''
})
class MockStartScreenComponent { }

@Component({
  selector: 'tsb-save-score',
  template: ''
})
class MockSaveScoreComponent { @Input() scoreToSave: 100; }

@Component({
  selector: 'tsb-game-state',
  template: ''
})
class MockGameStateComponent { }

describe('AppComponent', () => {
  beforeEach(async(() => {
    const gameServiceStub = {
    };

    const scoresServiceStub = {
      highScores: []
    };

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent,
        MockGameStateComponent,
        MockGameAreaComponent,
        MockSaveScoreComponent,
        MockStartScreenComponent
      ],
      providers: [
        { provide: GameService, useValue: gameServiceStub },
        { provide: ScoresService, useValue: scoresServiceStub }]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
