import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { GameStateComponent } from './game-state/game-state.component';
import { GameAreaComponent } from './game-area/game-area.component';

import { GameService, GameShapeService, ScoresService } from './services';
import { StartScreenComponent } from './start-screen/start-screen.component';
import { SaveScoreComponent } from './save-score/save-score.component';

@NgModule({
  declarations: [
    AppComponent,
    GameStateComponent,
    GameAreaComponent,
    StartScreenComponent,
    SaveScoreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [GameService, GameShapeService, ScoresService],
  bootstrap: [AppComponent]
})
export class AppModule { }
