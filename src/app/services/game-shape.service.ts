import { Injectable } from '@angular/core';
import { GameShape, GameBlock } from '../model';
@Injectable()
export class GameShapeService {

  private shapes = [
    [ // Line
      { x: 1, y: 0, colour: 'yellow' },
      { x: 1, y: 1, colour: 'yellow' },
      { x: 1, y: 2, colour: 'yellow' },
      { x: 1, y: 3, colour: 'yellow' }]
    ,
    [ // Block
      { x: 1, y: 1, colour: 'red' },
      { x: 2, y: 1, colour: 'red' },
      { x: 1, y: 2, colour: 'red' },
      { x: 2, y: 2, colour: 'red' }]
    ,
    [ // L
      { x: 1, y: 0, colour: 'blue' },
      { x: 1, y: 1, colour: 'blue' },
      { x: 1, y: 2, colour: 'blue' },
      { x: 2, y: 2, colour: 'blue' }]
    ,
    [ // Backwards L
      { x: 2, y: 0, colour: 'green' },
      { x: 2, y: 1, colour: 'green' },
      { x: 2, y: 2, colour: 'green' },
      { x: 1, y: 2, colour: 'green' }]
    ,
    [ // Z
      { x: 1, y: 1, colour: 'white' },
      { x: 2, y: 1, colour: 'white' },
      { x: 2, y: 2, colour: 'white' },
      { x: 3, y: 2, colour: 'white' }]
    ,
    [ // Backwards Z
      { x: 3, y: 1, colour: 'orange' },
      { x: 2, y: 1, colour: 'orange' },
      { x: 2, y: 2, colour: 'orange' },
      { x: 1, y: 2, colour: 'orange' }]
    ,
    [ // T
      { x: 1, y: 2, colour: 'orange' },
      { x: 2, y: 2, colour: 'orange' },
      { x: 3, y: 2, colour: 'orange' },
      { x: 2, y: 3, colour: 'orange' }]

  ];

  constructor() { }

  getShape(): GameShape {
    // Pick a random shape.
    return new GameShape(
      JSON.parse(
        JSON.stringify(
          this.shapes[Math.floor((Math.random() * this.shapes.length))])));
  }
}
