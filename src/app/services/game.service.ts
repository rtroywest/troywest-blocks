import { Injectable } from '@angular/core';
import { GameShape, GameBlock } from '../model';
import { GameShapeService } from './game-shape.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class GameService {
  public nextShape: GameShape;
  public gameOver$: Observable<number>;
  public gameStarted$: Observable<any>;

  private currentShape: GameShape;
  private timeout;
  private stepTime: number;
  private fallingShapeOffsetX: number;
  private fallingShapeOffsetY: number;
  private score = 0;
  private _settledBlocks: GameBlock[] = [];
  private BOARD_WIDTH = 8;
  private BOARD_HEIGHT = 16;
  private gameOverSubject = new Subject<number>();
  private gameStartedSubject = new Subject();

  get settledBlocks(): GameBlock[] {
    return this._settledBlocks;
  }

  get currentScore(): number {
    return this.score;
  }

  get activeShape(): GameShape {
    return new GameShape(this.translateBlocks(this.currentShape.blocks, this.fallingShapeOffsetX, this.fallingShapeOffsetY));
  }

  start(): void {
    this.stepTime = 2000; // Very slow start...
    this._settledBlocks = [];
    this.setNextFallingShape();
    this.score = 0;
    this.gameStartedSubject.next();
    this.executeStep();
  }

  moveLeft() {
    if (this.shapeCanMoveLeft()) {
      this.fallingShapeOffsetX--;
    }
  }

  moveRight() {
    if (this.shapeCanMoveRight()) {
      this.fallingShapeOffsetX++;
    }
  }

  moveDown() {
    if (this.shapeCanMoveDown()) {
      this.fallingShapeOffsetY++;
    }
  }

  rotate() {
    this.currentShape.rotateClockwise();
    const fallingBlocks = this.translateBlocks(this.currentShape.blocks, this.fallingShapeOffsetX, this.fallingShapeOffsetY);

    for (let i = 0; i < fallingBlocks.length; i++) {
      if (
        this.isInCollision(fallingBlocks[i]) || // Hit another block
        // Hit left wall - TODO: move right after rotation if possible so we can rotate the block at the edge
        fallingBlocks[i].x < 0 ||
        // Hit right wall  - TODO: move right after rotation if possible so we can rotate the block at the edge
        fallingBlocks[i].x >= this.BOARD_WIDTH ||
        fallingBlocks[i].y >= this.BOARD_HEIGHT) { // hit bottom
        this.currentShape.rotateAntiClockwise(); // Un-rotate
        return;
      }
    }
  }

  constructor(private shapeFactory: GameShapeService) {
    this.nextShape = this.shapeFactory.getShape();
    this.gameOver$ = this.gameOverSubject.asObservable();
    this.gameStarted$ = this.gameStartedSubject.asObservable();
  }

  private translateBlocks(blocksToTranslate: GameBlock[], x, y): GameBlock[] {
    const result = [];

    for (let i = 0; i < blocksToTranslate.length; i++) {
      result.push({
        x: blocksToTranslate[i].x + x,
        y: blocksToTranslate[i].y + y,
        colour: blocksToTranslate[i].colour
      });
    }

    return result;
  }

  private isInCollision(block: GameBlock) {
    for (let i = 0; i < this._settledBlocks.length; i++) {
      if (block.x === this._settledBlocks[i].x && block.y === this._settledBlocks[i].y) {

        return true; // hit another settled block.
      }
    }

    return false;
  }

  private setNextFallingShape() {
    this.currentShape = this.nextShape;
    this.nextShape = this.shapeFactory.getShape();
    this.fallingShapeOffsetX = 3;

    let lowestFallingBlock = 0;
    for (let i = 0; i < this.currentShape.blocks.length; i++) {
      if (this.currentShape.blocks[i].y > lowestFallingBlock) {
        lowestFallingBlock = this.currentShape.blocks[i].y;
      }
    }

    // Move it ino place - not all shapes have a block on the bottom row - we want it to come into view cleanly
    this.fallingShapeOffsetY = -(lowestFallingBlock + 1);
  }

  private shapeCanMoveDown(): boolean {
    const newPosition = this.translateBlocks(this.currentShape.blocks, this.fallingShapeOffsetX, this.fallingShapeOffsetY + 1);
    let result = true;

    newPosition.forEach(fallingBlock => {
      if (fallingBlock.y >= this.BOARD_HEIGHT || this.isInCollision(fallingBlock)) {
        result = false; // Hit the bottom of the game area
        return;
      }
    });

    return result;
  }

  private shapeCanMoveLeft(): boolean {
    let result = true;
    const newPosition = this.translateBlocks(this.currentShape.blocks, this.fallingShapeOffsetX - 1, this.fallingShapeOffsetY);

    newPosition.forEach(fallingBlock => {
      if (fallingBlock.x < 0 || this.isInCollision(fallingBlock)) {
        result = false; // Hit the left had side or another block
        return;
      }
    });

    return result;
  }

  private shapeCanMoveRight(): boolean {
    let result = true;
    const newPosition = this.translateBlocks(this.currentShape.blocks, this.fallingShapeOffsetX + 1, this.fallingShapeOffsetY);

    newPosition.forEach(fallingBlock => {
      if (fallingBlock.x >= this.BOARD_WIDTH || this.isInCollision(fallingBlock)) {
        result = false; // Hit the bottom of the game area
        return;
      }
    });

    return result;
  }

  private isGameOver(): boolean {
    for (let i = 0, len = this._settledBlocks.length; i < len; i++) {
      if (this._settledBlocks[i].y <= 0) {
        return true;
      }
    }

    return false; // Nothing in the top row
  }

  private removeCompleteRows() {
    const initialAccumulator = [];
    for (let i = 0; i < this.BOARD_HEIGHT; i++) {
      initialAccumulator.push([]);
    }

    const rows = this._settledBlocks.reduce(
      (acc, current) => {
        if (current.y >= 0) { // ignroe blocks that haven't fallen into the grid yet.
          acc[current.y].push(current);
        }
        return acc;
      },
      initialAccumulator);

    let scoreMultiplier = 1;

    // Check all 15 rows for a full row.
    for (let i = 0; i < rows.length; i++) {
      if (rows[i].length === this.BOARD_WIDTH) {
        rows[i].forEach(toRemove => {
          this._settledBlocks.splice(this._settledBlocks.indexOf(toRemove), 1);
        });

        for (let rowToDrop = i - 1; rowToDrop > -1; rowToDrop--) {
          rows[rowToDrop].forEach(fallingBlock => fallingBlock.y++);
        }

        this.score += 10 * scoreMultiplier;
        scoreMultiplier *= 2;
      }
    }
  }

  private executeStep(): void {
    if (!this.shapeCanMoveDown()) {
      this._settledBlocks = this._settledBlocks.concat(
        this.translateBlocks(this.currentShape.blocks, this.fallingShapeOffsetX, this.fallingShapeOffsetY));

      this.setNextFallingShape();

      this.removeCompleteRows();

      if (this.isGameOver()) {
        this.stopGame();
        return; // Don't move any more blocks or reset timer - we're done.
      }
    }

    this.moveDown();

    this.timeout = setTimeout(() => {
      this.executeStep();
    }, Math.max(100, this.stepTime - (this.score)));
  }

  private stopGame() {
    clearTimeout(this.timeout);
    this.gameOverSubject.next(this.currentScore);
    // Do some score saving etc - probably put up some sort of "game over" overlay.
  }
}
