import { Injectable } from '@angular/core';
import { HighScore } from '../model';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ScoresService {
  private highScoresSubject: Subject<HighScore[]> = new Subject<HighScore[]>();

  public highScores$: Observable<HighScore[]>;
  public   highScores: HighScore[] = [];

  constructor() {
    this.highScores$ = this.highScoresSubject.asObservable();
  }

  refreshScores() {
    this.highScoresSubject.next(this.highScores);
    // TODO - store in the cloud and retreive from there
  }

  saveScore(name: string, score: number) {
    // TODO - store in the cloud and wait for response containing new high scores.
    let newHighScore = this.highScores.length <= 5; // If there's less than five then we have a new high score...
    for (let i = 0; i < this.highScores.length; i++) {
      if (this.highScores[i].score < score) {
        newHighScore = true;
        break;
      }
    }

    if (!newHighScore) {
      return;
    }

    this.highScores.push({
      score: score,
      name: name,
      time: new Date()
    });

    this.highScores.sort(function (x, y) { return y.score - x.score; });
    this.highScores = this.highScores.slice(0, 5);

    this.highScoresSubject.next(this.highScores);
  }
}
