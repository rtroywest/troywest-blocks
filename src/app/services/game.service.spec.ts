import { TestBed, inject } from '@angular/core/testing';

import { GameService, GameShapeService } from '.';
import { GameShape } from '../model';

describe('GameService', () => {
  const gameShapeServiceStub = {
    getShape() {
      return new GameShape([]);
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameService, { provide: GameShapeService, useValue: gameShapeServiceStub }]
    });
  });

  it('should be created', inject([GameService], (service: GameService) => {
    expect(service).toBeTruthy();
  }));
});
