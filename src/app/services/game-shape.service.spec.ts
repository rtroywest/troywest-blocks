import { TestBed, inject } from '@angular/core/testing';

import { GameShapeService } from './game-shape.service';

describe('GameShapeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameShapeService]
    });
  });

  it('should be created', inject([GameShapeService], (service: GameShapeService) => {
    expect(service).toBeTruthy();
  }));
});
