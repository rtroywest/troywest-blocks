import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameStateComponent } from './game-state.component';
import { GameService } from '../services';
import { GameShape } from '../model';

describe('GameStateComponent', () => {
  let component: GameStateComponent;
  let fixture: ComponentFixture<GameStateComponent>;

  beforeEach(async(() => {
    const gameServiceStub = {
      nextShape: new GameShape([]),
      currentScore: 0
    };

    TestBed.configureTestingModule({
      declarations: [ GameStateComponent ],
      providers:    [ {provide: GameService, useValue: gameServiceStub } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
