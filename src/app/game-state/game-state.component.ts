import { Component, OnInit } from '@angular/core';
import { GameShape } from '../model';
import { GameService } from '../services';

@Component({
  selector: 'tsb-game-state',
  templateUrl: './game-state.component.html',
  styleUrls: ['./game-state.component.css']
})
export class GameStateComponent implements OnInit {

  get currentScore(): number {
    return this.gameService.currentScore;
  }

  get nextShape(): GameShape {
    return this.gameService.nextShape;
  }

  constructor(private gameService: GameService) { }

  ngOnInit() {
  }

}
