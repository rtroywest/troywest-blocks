import { Component, OnInit, HostListener } from '@angular/core';
import { GameBlock } from '../model';
import { GameService } from '../services';

@Component({
  selector: 'tsb-game-area',
  templateUrl: './game-area.component.html',
  styleUrls: ['./game-area.component.css']
})
export class GameAreaComponent implements OnInit {

  constructor(private gameService: GameService) { }

  blocks(): GameBlock[] {
    // TODO - look into the ramifications of returning a new array every time.
    // Might be an issue with changes being triggered for every binding when not requried.
    return this.gameService.settledBlocks.concat(this.gameService.activeShape.blocks);
  }

  ngOnInit() {
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(ev: KeyboardEvent) {
    switch (ev.keyCode) {
      case 37:
        this.gameService.moveLeft();
        break;
      case 39:
        this.gameService.moveRight();
        break;
      case 40:
        this.gameService.moveDown();
        break;
        case 32:
        this.gameService.rotate();
        break;
    }
  }
}
