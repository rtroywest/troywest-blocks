import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameAreaComponent } from './game-area.component';
import { GameService } from '../services';

import { GameShape } from '../model';

describe('GameAreaComponent', () => {
  let component: GameAreaComponent;
  let fixture: ComponentFixture<GameAreaComponent>;

  beforeEach(async(() => {
    const gameServiceStub = {
      settledBlocks: [],
      activeShape: new GameShape([])
    };
    TestBed.configureTestingModule({
      declarations: [GameAreaComponent],
      providers: [{ provide: GameService, useValue: gameServiceStub }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
