import { Component, OnInit } from '@angular/core';
import { GameService, ScoresService } from './services';
import { HighScore } from './model';

@Component({
  selector: 'tsb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public activeScreen = 'Start';
  public lastScore = 0;
  public bestScore = 0;

  public highScores: HighScore[] = [];
  constructor(private gameService: GameService, private scoresService: ScoresService) {

  }

  ngOnInit() {
    this.gameService.gameStarted$.subscribe(() => this.activeScreen = 'Playing');
    this.gameService.gameOver$.subscribe(score => {
      this.lastScore = score;
      this.bestScore = Math.max(score, this.bestScore);
      this.activeScreen = 'SaveScore';
    });

    this.scoresService.refreshScores();
    this.scoresService.highScores$.subscribe(scores => this.highScores = scores);
  }

  onScoreSaved() {
    this.activeScreen = 'Start';
  }
}
