import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { SaveScoreComponent } from './save-score.component';
import { ScoresService } from '../services';

describe('SaveScoreComponent', () => {
  let component: SaveScoreComponent;
  let fixture: ComponentFixture<SaveScoreComponent>;

  beforeEach(async(() => {
    const scoresServiceStub = {};

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [SaveScoreComponent],
      providers: [{ provide: ScoresService, useValue: scoresServiceStub }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
