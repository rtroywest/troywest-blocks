import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ScoresService } from '../services';

@Component({
  selector: 'tsb-save-score',
  templateUrl: './save-score.component.html',
  styleUrls: ['./save-score.component.css']
})
export class SaveScoreComponent implements OnInit {
  @Output() saved: EventEmitter<any> = new EventEmitter();
  @Input() scoreToSave = 0;

  userName: '';

  constructor(private scoresService: ScoresService) { }

  ngOnInit() {
  }

  saveScore() {
    this.scoresService.saveScore(this.userName, this.scoreToSave);
    this.saved.next({});
  }
}
