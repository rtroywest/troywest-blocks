export class HighScore {
    name: string;
    time: Date;
    score: number;
}
