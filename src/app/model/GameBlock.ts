export class GameBlock {
    public x: number;
    public y: number;
    public colour: string;
}
