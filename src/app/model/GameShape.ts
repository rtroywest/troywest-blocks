import { GameBlock } from './GameBlock';

export class GameShape {
    constructor(public blocks: GameBlock[]) {
    }

    rotateClockwise() {
        for (let i = 0; i < this.blocks.length; i++) {
            const newY = this.blocks[i].x;
            const newX = 3 - this.blocks[i].y;
            this.blocks[i].x = newX;
            this.blocks[i].y = newY;
        }
    }

    rotateAntiClockwise() {
        for (let i = 0; i < this.blocks.length; i++) {
            const newX = this.blocks[i].y;
            const newY = 3 - this.blocks[i].x;
            this.blocks[i].x = newX;
            this.blocks[i].y = newY;
        }
    }
}
